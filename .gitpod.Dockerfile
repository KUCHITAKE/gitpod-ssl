FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update && \
    sudo apt-get install -y -qq tzdata && \
    sudo apt-get install -y -qq git && \
    sudo apt-get install -y libgtk-3-dev && \ 
    sudo apt-get install -y libgl1-mesa-dev xorg-dev && \
    sudo apt-get install -y git build-essential cmake pkg-config qt5-default \
        libqt5opengl5-dev libgl1-mesa-dev libglu1-mesa-dev \
        libprotobuf-dev protobuf-compiler libode-dev libboost-dev \
        g++ libeigen3-dev libboost1.71-dev qtbase5-dev libqt5serialport5-dev libprotobuf-dev libprotoc-dev protobuf-compiler ninja-build \
        doxygen graphviz

WORKDIR /workspace
RUN sudo git clone  --depth 1 https://github.com/RoboCup-SSL/grSim.git

